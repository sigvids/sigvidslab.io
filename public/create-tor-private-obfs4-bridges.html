<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Create Tor Private Obfs4 Bridges</title>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<h1>How to Create Tor Private Obfs4 Bridges</h1>
<p>To create a private obfs4 bridge, you will need a virtual private server (VPS) running Debian 10.</p>

<h2>1. Install Firewall</h2>
<p>After you first create your server, update all the existing packages:</p>
<pre>sudo apt update

sudo apt upgrade</pre>
<p>Then install Nftables to implement your server&rsquo;s firewall:</p>
<pre>sudo apt install nftables

sudo systemctl enable nftables

sudo systemctl start nftables</pre>
<p>Now add the rules. In the rules below:
<ul>
<li>For SSH on port 22, replace <code>YOUR.PC.IP.ADDRESS</code> with your actual PC IP address 
(or a range of IP addresses, such as <code>YOUR.PC.0.0/16</code>, if your IP address changes from time to time but always falls within a range).</li>
<li>Also replace the choice of port to reach the obfs4 bridge, which in our example below was chosen to be port <code>443</code>, 
with your own choice of port number.</li>
<li>Note that the firewall is <em>not</em> opened for general Tor traffic on the ORPort. 
This is to prevent the server from responding to scans for Tor relays, pending the completion of
<a href="https://trac.torproject.org/projects/tor/ticket/7349" target="_blank">#7349</a>.</li>
<li>The escape character (backslash) appears in the commands below to escape a line break. You can, if you wish, omit the escape character
and enter the commands as a single line.</li>
</ul>
<pre>sudo nft add rule inet filter input ct state related,established \
    counter accept

sudo nft add rule inet filter input iif lo counter accept

sudo nft add rule inet filter input tcp dport 22 \
    ip saddr YOUR.PC.IP.ADDRESS counter accept
    
sudo nft add rule inet filter input tcp dport 443 counter accept</pre>
<p>Drop any unexpected traffic:</p>
<pre>sudo nft add rule inet filter input counter drop</pre>
<p>Save these rules so that they persist across reboots:</p>
<pre>sudo su -c 'nft list ruleset > /etc/nftables.conf'</pre>

<h2>2. Install Tor</h2>
<p>Install the prerequisite package:</p>
<pre>sudo apt install apt-transport-https</pre>
<p>Add the Tor repositories to your Advanced Packaging Tool (APT) sources list:</p>
<pre>sudo vi /etc/apt/sources.list</pre>
<p>Press <strong>Shift</strong>+<strong>g</strong> to navigate to the bottom of the file.
Press the <strong>o</strong> key to open lines for input.
Add lines at the bottom for the Tor project repositories:</p>
<pre>deb https://deb.torproject.org/torproject.org buster main
deb-src https://deb.torproject.org/torproject.org buster main</pre>
<p>Press the <strong>Esc</strong> key on your computer keyboard to escape from insert mode.
Type <strong>:wq</strong> and press <strong>Enter</strong>
to write the file to disk and quit the editor.</p>
<p>Add the GNU Privacy Guard (GPG) key used to sign the Tor packages.
The escape character (backslash) appears in the commands below to escape a line break. 
You can, if you wish, omit the escape character
and enter the commands as a single line.</p>
<pre>sudo apt install gpg

wget -qO- https://deb.torproject.org/torproject.org/\
A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89.asc | gpg --import

gpg --export A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89 \
    | sudo apt-key add -</pre>
<p>Update your package lists:</p>
<pre>sudo apt update</pre>
<p>Install Tor and the Tor Debian keyring from the Tor project repository:</p>
<pre>sudo apt install tor deb.torproject.org-keyring</pre>

<h2>3. Install Obfs4proxy</h2>
<p>Install the package for obfs4:</p>
<pre>sudo apt install obfs4proxy</pre>
<p>Allow obfs4proxy to bind to privileged ports:</p>
<pre>sudo setcap cap_net_bind_service=+ep /usr/bin/obfs4proxy</pre>
<p>While <a href="https://trac.torproject.org/projects/tor/ticket/18356">#18356</a> 
is still outstanding, you must also amend two Systemd service files as follows.</p>
<p>Edit the default service file:</p>
<pre>sudo vi /lib/systemd/system/tor@default.service</pre>
<p>Use the <strong>Page Down</strong> key on your keyboard. 
Use the down arrow key to reach the line that says <code>NoNewPrivileges=yes</code>.
Press the <strong>w</strong> key twice to go forward to the word <code>yes</code>.
Use the <strong>x</strong> key three times to delete three characters.
Press the <strong>a</strong> key to append characters.
Change the line to read:</p>
<pre>NoNewPrivileges=no</pre>
<p>Press the <strong>Esc</strong> key on your computer keyboard to escape from insert mode.
Type <strong>:wq</strong> and press <strong>Enter</strong>
to write the file to disk and quit the editor.</p>
<p>Edit the service file:</p>
<pre>sudo vi /lib/systemd/system/tor@.service</pre>
<p>Where it says <code>NoNewPrivileges=yes</code>, repeat the process to change it to read:</p>
<pre>NoNewPrivileges=no</pre>
<p>Type <strong>:wq</strong> and press <strong>Enter</strong>
to write the file to disk and quit the editor.</p>
<p>Then run:</p>
<pre>sudo systemctl daemon-reload</pre>

<h2>4. Configure Tor</h2>
<p>Edit the Tor configuration file:</p>
<pre>sudo vi /etc/tor/torrc</pre>
<p>Press the keys <strong>dG</strong> on your computer keyboard to delete the existing lines.
Press the <strong>i</strong> key to enter insert mode.
Insert the lines that follow. Note that:</p>
<ul>
<li>Although the <code>ORPort</code> is set to the default of <code>9001</code>, that port is <em>not</em> open in the firewall, 
and so the server is <em>not</em> open to probing.</li>
<li>We specify <code>PublishServerDescriptor 0</code> in the sample configuration file below, 
but you can alternatively specify <code>BridgeDistribution none</code>. 
That will make your bridge publish its statistics in the metrics dataset
while preventing BridgeDB from giving out the bridge.</li>
<li>The line <code>ExitPolicy reject *:*</code> is an optional safeguard,
since it should be redundant with setting <code>BridgeRelay 1</code>.</li>
<li>Replace port <code>443</code> with your choice of port number for incoming obfuscated connections.</li>
<li>You must replace <code>yourname@example.com</code> with your actual email address.</li>
<li>Also replace <code>PonteVecchio</code> with your choice for a bridge nickname.</li>
</ul>

<pre>Log notice file /var/log/tor/log
ORPort 9001
AssumeReachable 1
ExtORPort auto
BridgeRelay 1
PublishServerDescriptor 0
ExitPolicy reject *:*
ServerTransportPlugin obfs4 exec /usr/bin/obfs4proxy
ServerTransportListenAddr obfs4 0.0.0.0:443
ContactInfo yourname@example.com
Nickname PonteVecchio</pre>
<p>Press the <strong>Esc</strong> key on your computer keyboard to escape from insert mode.
Type <strong>:wq</strong> and press <strong>Enter</strong>
to write the file to disk and quit the editor.</p>

<h2>5. Start Bridge</h2>
<p>Start your bridge running with your revised configuration by issuing the command:</p>
<pre>sudo systemctl restart tor</pre>

<h2>6. Construct Bridge Line</h2>
<p>Obtain a template for the bridge line:</p>
<pre>sudo cat /var/lib/tor/pt_state/obfs4_bridgeline.txt</pre>
<p>The template will look like this:</p>
<pre>Bridge obfs4 &lt;IP ADDRESS&gt;:&lt;PORT&gt; &lt;FINGERPRINT&gt; \
cert=KkdWiWlfetJG9SFrzX8g1teBbgxtsc0zPiN5VLxqNNH+iudVW48CoH/XVXPQntbivXIqZA \
iat-mode=0</pre>
<p>Obtain your bridge&rsquo;s fingerprint:
<pre>sudo cat /var/lib/tor/fingerprint</pre>
<p>The results will show your bridge nickname and its fingerprint:</p>
<pre>PonteVecchio EFC6A00EE6272355C023862378AC77F935F091E4</pre>
<p>Substitute your server&rsquo;s IP address, your chosen port, and your fingerprint into the bridge line. 
The results will look like this:</p>
<pre>Bridge obfs4 123.45.67.89:443 EFC6A00EE6272355C023862378AC77F935F091E4 \
cert=KkdWiWlfetJG9SFrzX8g1teBbgxtsc0zPiN5VLxqNNH+iudVW48CoH/XVXPQntbivXIqZA \
iat-mode=0</pre>

<h2>7. Test Bridge</h2>
<h3>a) Check Tor Log</h3>
<p>Check the Tor log file:</p>
<pre>sudo cat /var/log/tor/log</pre>
<p>You should see a line:</p>
<pre>Bootstrapped 100% (done): Done</pre>
<h3>b) Test TCP Connectivity</h3>
<p>Open Windows PowerShell and issue the following command.
Replace <code>123.45.67.89</code> by your server&rsquo;s actual IP address
and <code>443</code> by your choice of port.</p>
<pre>Test-NetConnection -ComputerName "123.45.67.89" -Port 443 -InformationLevel "Detailed"</pre>
<p>After a minute or so, you should see a line:</p>
<pre>TcpTestSucceeded        : True</pre>
<h3>c) Test from Tor Browser on Windows</h3>
<p>Test your bridge from the Tor Browser for Windows, as described in the article
<a href="use-tor-private-obfs4-bridges-windows.html" target="_blank">How to Use 
Tor Private Obfs4 Bridges from Windows</a>.</p>

<h2>8. Communicate Bridge Line to User(s)</h2>
<p>Communicate the bridge line to your user(s) by a secure method
such as <a href="https://signal.org" target="_blank">Signal</a> or 
<a href="https://telegram.org" target="_blank">Telegram</a> messengers or from a secure website.
Note that email is normally <em>not</em> secure.</p>
<p>Signal and Telegram are blocked in several countries. 
In some cases it may be possible to connect to Telegram using an MTProto proxy.
Describing how to use a proxy in Telegram is out of scope for this article.</p>
<p>There is a decision to be made as to whether you install and configure software
before or after you enter a censored country.</p>
<p>There is a trade-off here between convenience and security.
It is more convenient to install software and configurations in a free country.
The risk is that electronic devices may be inspected when entering a censored country.
On the other hand, installing software and configurations after arrival
may necessitate additional circumvention measures to reach blocked resources.
In the worst case, a user may be completely unable to download the required
software and configurations.</p>

<h2>9. Optionally Install Nyx</h2>
<p>If you want to monitor your bridge, you can optionally install the Nyx command-line monitor, previously known as the anonymizing relay monitor (arm):</p>
<pre>sudo apt install python3-pip

sudo pip3 install nyx</pre>
<p>Edit your Tor configuration file:</p>
<pre>sudo vi /etc/tor/torrc</pre>
<p>Add the lines:</p>
<pre>ControlPort 9051
CookieAuthentication 1</pre>
<p>Press the <strong>Esc</strong> key on your computer keyboard to escape from insert mode.
Type <strong>:wq</strong> and press <strong>Enter</strong>
to write the file to disk and quit the editor.</p>
<p>Start your bridge running with your revised configuration by issuing the command:</p>
<pre>sudo systemctl restart tor</pre>
<p>Add yourself to the <code>debian-tor</code> group.
In the command that follows, replace <code>yourusername</code> by your actual username:</p>
<pre>sudo usermod -a -G debian-tor yourusername</pre>
<p>Log off and then log on again so that the above command becomes effective.</p>
<p>Invoke Nyx with the command:</p>
<pre>nyx</pre>
<p>Use the left and right arrows on your computer keyboard to page through the Nyx screens. Press <strong>q</strong> and then <strong>q</strong> again to quit Nyx.</p>

</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Create Tor Public Obfs4 Bridges</title>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>

<h1>How to Create Tor Public Obfs4 Bridges</h1>
<p>This page shows you how to create a public obfs4 bridge. By making a bridge "public" you allow the Tor Project to share your bridge with users from around the world.</p>
<p>The article here expands on the article on the Tor Project website about <a href="https://community.torproject.org/relay/setup/bridge/debian-ubuntu" target="_blank">setting up a bridge on Debian/Ubuntu</a>. The purpose of this expansion is to include some details and explanations missing from the original article. For ease of reference, we use the same section numbering:</p>
<ol>
<li>Enable Automatic Software Updates</li>
<li>Configure Tor Project Repository</li>
<li>Install Tor</li>
<li>Install Obfs4proxy</li>
<li>Edit Your Tor Config File</li>
<li>Restart Tor</li>
<li>Monitor Your Logs</li>
<li>Final Notes</li>
</ol>

<h2>0. Preparation</h2>
<h3>a. Rent VPS</h3>
<p>To create a public obfs4 bridge, you will need a virtual private server (VPS). You can rent a VPS from many providers. Some of the most popular ones are <a href="https://us.ovhcloud.com" target="_blank">OVH</a>, <a href="https://www.hetzner.com" target="_blank">Hetzner</a>, and <a href="https://www.digitalocean.com" target="_blank">DigitalOcean</a>. Of course, these are fine, but you can increase the diversity of the Tor network by choosing a provider not on the list of the most popular ones. You'll need to consult your provider's Acceptable Use Policy to make sure the provider allows Tor (non-exit) nodes. Choose a provider with a large monthly bandwidth allowance. Your VPS should use KVM or Xen virtualization (but not OpenVZ). You'll need a public IPv4 address. Bridges can have 512 MB of RAM, but ideally your VPS will have 1 GB or more of RAM. Tor does not need much disk storage, so disk space will probably not be a limiting factor. For operating system, choose Debian 10 or Debian 11.</p>
<h3>b. SSH into VPS</h3>
<p>You can SSH into your server using an SSH client such as a terminal emulator, Windows PowerShell, PuTTY, or XSHELL. Ideally you set up your server with an SSH public key on the server and an SSH private key on the client. If you are not using public key authentication, make sure you choose a strong password.</p>
<p>In the commands in this tutorial, we assume you log in as <code>root</code>. If you are not <code>root</code>, then either prefix commands by <code>sudo</code>, or else switch to the root user (<code>sudo su -</code>) before you start work.</p>
<h3>c. Update VPS</h3>
<p>After you first create your server, update all the existing packages:</p>
<pre>apt update && apt upgrade</pre>
<h3>d. Install a Firewall on VPS</h3>
<p>There are many ways to add a firewall to your server. In this tutorial, you will use <code>iptables</code> to implement the firewall. Note that some VPS providers implement security groups outside of the server itself in their server management web application.</p>
<p>In the <code>iptables</code> rules below:
<ul>
<li>For SSH on port 22, replace <code>YOUR.PC.IP.ADDRESS</code> with your actual PC IP address 
(or a range of IP addresses, such as <code>YOUR.PC.0.0/16</code>, if your IP address changes from time to time but always falls within a range). If you do not know your IP address, visit <a href="https://whatismyipaddress.com" target="_blank">https://whatismyipaddress.com</a> to determine it.</li>
<li>Also replace the choice of port to reach the obfs4 bridge, which in our example below was chosen to be port <code>8888</code>, 
with your own choice of port number.</li>
<li>Replace the OR Port, which in our example below is port <code>9999</code>, 
with your own choice of port number. This is the port the rest of the Tor network uses to communicate with your bridge.</li>
</ul>
<p>Here are the <code>iptables</code> rules to implement a basic firewall to protect your server from unauthorized access:</p>
<pre>
iptables -A INPUT -m conntrack --ctstate INVALID -j DROP
iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
iptables -A INPUT -i lo -j ACCEPT
iptables -A INPUT -p icmp -j ACCEPT
iptables -A INPUT -p tcp -s YOUR.PC.IP.ADDRESS --dport 22 -j ACCEPT
iptables -A INPUT -p tcp --dport 8888 -j ACCEPT
iptables -A INPUT -p tcp --dport 9999 -j ACCEPT
iptables -P INPUT DROP</pre>
<p>The next set of rules block IPv6 input. You will need to change these if you actually want to use IPv6 as well as IPv4 for your ORPort.</p>
<pre>ip6tables -A INPUT -m conntrack --ctstate INVALID -j DROP
ip6tables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
ip6tables -A INPUT -i lo -j ACCEPT
ip6tables -A INPUT -p icmpv6 -j ACCEPT
ip6tables -P INPUT DROP
</pre>
<p>Save these rules so that they persist across reboots:</p>
<pre>apt install iptables-persistent</pre>

<h2>1. Enable Automatic Software Updates</h2>
<p>You will need to keep your server up to date by issuing the commands <code>apt update && apt upgrade</code> periodically, for example every week or every month. However, there may be urgent security upgrades in between your manual updates. We will use the <code>unattended-upgrades</code> package to apply urgent upgrades within 24 hours. In addition, we will install the package <code>apt-listchanges</code> to compare new versions with previous ones and report on what has changed.</p>
<p>Install the packages:</p>
<pre>apt install unattended-upgrades apt-listchanges</pre>
<p>Edit the default configuration file:</p>
<pre>vi /etc/apt/apt.conf.d/50unattended-upgrades</pre>
<p>Change the <code>Origins-Pattern</code> section to specify Debian security upgrades and Tor Project upgrades:</p>
<pre>Unattended-Upgrade::Origins-Pattern {
    "origin=Debian,codename=${distro_codename},label=Debian-Security";
    "origin=TorProject";
};</pre>
<p>The list of packages to exclude should be empty:</p>
<pre>Unattended-Upgrade::Package-Blacklist {
};</pre>
<p>Do <strong>:wq</strong> on your computer keyboard and press <strong>Enter</strong> to write the file to disk and quit the editor.</p>
<p>To activate <code>unattended-upgrades</code>, edit the apt configuration stub:</p>
<pre>vi /etc/apt/apt.conf.d/20auto-upgrades</pre>
<p>Edit the file so that you do <code>apt-get update</code> automatically every 1 days; do <code>apt-get autoclean</code> every 5 days; run the <code>unattended-upgrade</code> upgrade script every 1 day; and show verbose output:</p>
<pre>APT::Periodic::Update-Package-Lists "1";
APT::Periodic::AutocleanInterval "5";
APT::Periodic::Unattended-Upgrade "1";
APT::Periodic::Verbose "1";</pre>
<p>Do <strong>:wq</strong> on your computer keyboard and press <strong>Enter</strong> to write the file to disk and quit the editor.</p>
<p>Test <code>unattended-upgrade</code> manually with the debug option (<code>-d</code>):</p>
<pre>unattended-upgrade -d</pre>
<p>Display the results, if any:</p>
<pre>cat /var/log/unattended-upgrades/unattended-upgrades.log</pre>
<p>Unattended-upgrades is now configured. Note that you will still need to issue the commands <code>apt update && apt upgrade</code> periodically for all other, non-urgent upgrades.</p>

<h2>2. Configure Tor Project Repository</h2>
<p>Install the prerequisite packages:</p>
<pre>apt install apt-transport-https gpg</pre>
<p>Add the Tor repositories to your Advanced Packaging Tool (APT) sources lists:</p>
<pre>vi /etc/apt/sources.list.d/tor.list</pre>
<p>Press the <strong>i</strong> key on your keyboard to enter insert mode. Add lines for Tor Project repositories.</p>
<p>For Debian 10 "buster":</p>
<pre>deb [signed-by=/usr/share/keyrings/tor-archive-keyring.gpg] https://deb.torproject.org/torproject.org buster main
deb-src [signed-by=/usr/share/keyrings/tor-archive-keyring.gpg] https://deb.torproject.org/torproject.org buster main</pre>
<p>For Debian 11 "bullseye":</p>
<pre>deb [signed-by=/usr/share/keyrings/tor-archive-keyring.gpg] https://deb.torproject.org/torproject.org bullseye main
deb-src [signed-by=/usr/share/keyrings/tor-archive-keyring.gpg] https://deb.torproject.org/torproject.org bullseye main</pre>
<p>Press the <strong>Esc</strong> key to leave insert mode. Do <strong>:wq</strong> and press <strong>Enter</strong> to write the file to disk and quit the editor.</p>
<p>Add the GNU Privacy Guard (GPG) key used to sign the Tor packages.
The escape character (backslash) appears in the command below to escape a line break. 
You can, if you wish, omit the escape character
and enter the command as a single line.</p>
<pre>wget -O- https://deb.torproject.org/torproject.org/\
A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89.asc \
| gpg --dearmor \
| tee /usr/share/keyrings/tor-archive-keyring.gpg >/dev/null</pre>
<p>Update your package lists for the new repositories:</p>
<pre>apt update</pre>

<h2>3. Install Tor</h2>
<p>Install Tor and the Tor Debian keyring from the Tor project repository:</p>
<pre>apt install tor deb.torproject.org-keyring</pre>
<p>Check the version of Tor that was installed:</p>
<pre>tor --version</pre>

<h2>4. Install Obfs4proxy</h2>
<p>Install the package for obfs4:</p>
<pre>apt install obfs4proxy</pre>

<h2>5. Edit Your Tor Config File</h2>
<p>Edit the Tor configuration file:</p>
<pre>vi /etc/tor/torrc</pre>
<p>Press the keys <strong>dG</strong> on your computer keyboard to delete the existing lines.
Press the <strong>i</strong> key to enter insert mode.
Insert the lines that follow. Note that:</p>
<ul>
<li><code>ORPort</code> is set to <code>9999</code>, but you can change this to your own chosen value.</li>
<li>The line <code>ExitPolicy reject *:*</code> is an optional safeguard,
since it should be redundant with setting <code>BridgeRelay 1</code>.</li>
<li>The bridge listening port is set to <code>8888</code>, but you can change this to your own chosen value.</li>
<li>Insert your own email address. The square brackets are an anti-spam measure indicating where the at-sign <code>@</code> should go.</li>
<li>Insert your own choice of a nickname for your bridge of up to 19 alphanumeric characters.</li>
<li>Change the monthly limit of <code>1000 GB</code> as necessary. Note that Tor applies the limit to sending and receiving separately. You will need to determine whether your VPS provider's limits apply to sending or receiving or both added together.</li>
<li>The control port is set to <code>9051</code> but this port is not open in the firewall, so that only a local user on the server itself can control the Tor process using the Tor Control Protocol.</li>
</ul>
<pre>Log notice file /var/log/tor/log
ORPort 9999 IPv4Only
ExtORPort auto
BridgeRelay 1
PublishServerDescriptor bridge
ExitPolicy reject *:*
ServerTransportPlugin obfs4 exec /usr/bin/obfs4proxy
ServerTransportListenAddr obfs4 0.0.0.0:8888
ContactInfo youremail[]yourdomain.com
Nickname ChooseNameHere
AccountingMax 1000 GB
ControlPort 9051
CookieAuthentication 1</pre>
<p>Press the <strong>Esc</strong> key on your computer keyboard to escape from insert mode.
Type <strong>:wq</strong> and press <strong>Enter</strong> to write the file to disk and quit the editor.</p>

<h2>6. Restart Tor</h2>
<p>Start your bridge running with your revised configuration by issuing the command:</p>
<pre>systemctl restart tor@default</pre>

<h2>7. Monitor Your Logs</h2>
<p>To confirm your bridge is active:</p>
<pre>tail /var/log/tor/log</pre>
<p>You should see a message <code>Self-testing indicates your ORPort is reachable from the outside. Excellent. Publishing server descriptor.</code></p>

<h2>8. Final Notes</h2>
<p>The steps in this section are optional. You'll see how to:</p>
<ul>
<li>test your obfs4 bridge</li>
<li>check its Tor metrics page</li>
<li>monitor utilization of your bridge in real time</li>
</ul>
<h3>a. Test Your Obfs4 Bridge</h3>
<p>To test your obfs4 bridge from your own Tor Browser, first obtain a template for the bridge line:</p>
<pre>cat /var/lib/tor/pt_state/obfs4_bridgeline.txt</pre>
<p>The template will look like this:</p>
<pre>Bridge obfs4 &lt;IP ADDRESS&gt;:&lt;PORT&gt; &lt;FINGERPRINT&gt; \
cert=KkdWiWlfetJG9SFrzX8g1teBbgxtsc0zPiN5VLxqNNH+iudVW48CoH/XVXPQntbivXIqZA \
iat-mode=0</pre>
<p>Also obtain your bridge&rsquo;s fingerprint:
<pre>cat /var/lib/tor/fingerprint</pre>
<p>The results will show your bridge nickname and its fingerprint:</p>
<pre>ChooseNameHere EFC6A00EE6272355C023862378AC77F935F091E4</pre>
<p>Substitute your server&rsquo;s IP address, your chosen obfs4 port (<code>8888</code> in our example), and your fingerprint into the bridge line. The results will look like this:</p>
<pre>Bridge obfs4 123.45.67.89:8888 EFC6A00EE6272355C023862378AC77F935F091E4 \
cert=KkdWiWlfetJG9SFrzX8g1teBbgxtsc0zPiN5VLxqNNH+iudVW48CoH/XVXPQntbivXIqZA \
iat-mode=0</pre>
<p>Now launch Tor Browser on your PC. Reconfigure Tor Network Settings to use your own obfs4 bridge. Test to see whether you can connect to <a href="https://check.torproject.org"= target="_blank">https://check.torproject.org</a>.</p>
<h3>b. Check Tor Metrics Page</h3>
<p>After about 3 hours, your bridge will be searchable by fingerprint (but <em>not</em> by IP address) at <a href="https://metrics.torproject.org/rs.html#search"= target="_blank">https://metrics.torproject.org/rs.html#search</a>.</p>
<p>You can also check that your bridge appears to be functional by opening a browser and visiting <code>https://bridges.torproject.org/status?id=FINGERPRINT</code>, replacing FINGERPRINT by your bridge's fingerprint.</p>
<h3>c. Monitor Utilization in Real Time</h3>
<p>If you want to monitor utilization of your bridge in real time, you can optionally install the Nyx command-line monitor, previously known as the anonymizing relay monitor (arm):</p>
<pre>apt install python3-pip

pip3 install nyx</pre>
<p>Invoke Nyx with the command:</p>
<pre>nyx</pre>
<p>Use the left and right arrows on your computer keyboard to page through the Nyx screens. Press <strong>q</strong> and then <strong>q</strong> again to quit Nyx.</p>

</body>
</html>
